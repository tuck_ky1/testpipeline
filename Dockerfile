# Use the official OpenJDK base image
FROM openjdk:17

# Set the working directory in the container
WORKDIR /app

# Copy the packaged jar file into the container at /app
COPY target/testpipeline-0.0.1-SNAPSHOT.jar /app/testpipeline-0.0.1-SNAPSHOT.jar

# Expose the port the app runs on
EXPOSE 8080

# Specify the command to run your Spring Boot application
CMD ["java", "-jar", "testpipeline-0.0.1-SNAPSHOT.jar"]